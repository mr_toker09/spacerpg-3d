﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeaconController : MonoBehaviour
{
    public Signal beaconPositionSignal;
    public Signal beaconDistressSignal;
    public float timeToScan;

    private bool posSignalSent;
    private float t;
    private MapScreenController _mapScreenController;

    private void Awake()
    {
        _mapScreenController = FindObjectOfType<MapScreenController>();
    }

    // Start is called before the first frame update
    void Start()
    {
        posSignalSent = false;
        t = 0f;
        Invoke("RaiseDistressSignal", 10f);
    }

    // Update is called once per frame
    void Update()
    {
        if (_mapScreenController.Scanning)
        {
            t += Time.deltaTime;
            if (t >= timeToScan)
            {
                RaisePositionSignal();
            }
        }
        else
        {
            t = 0f;
        }
    }

    void RaiseDistressSignal()
    {
        beaconDistressSignal.Raise();
    }

    void RaisePositionSignal()
    {
        if (posSignalSent)
            return;

        beaconPositionSignal.Raise();
        posSignalSent = true;
    }
}
