﻿using UnityEngine;

public class ObjectDetector : MonoBehaviour
{
    public Signal objectInSightSignal;
    public Signal noObjectInSightSignal;

    [HideInInspector]
    public string nameStr;
    [HideInInspector]
    public string typeStr;
    [HideInInspector]
    public string descStr;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0f));
        if (Physics.Raycast(ray, out hit, 25000f))
        {
            if (hit.transform.gameObject.CompareTag("Celestial"))
            {
                CelestialInfo _cInfo = hit.transform.gameObject.GetComponent<CelestialInfo>();
                objectInSightSignal.Raise();
                UpdateCelestialInfo(_cInfo.celestial.Name, _cInfo.celestial.Type, _cInfo.celestial.Description);
                objectInSightSignal.Raise();
            }
            Debug.Log("You hit the " + hit.transform.name); // ensure you picked right object
        }
        else
        {
            UpdateCelestialInfo(string.Empty, string.Empty, string.Empty);
            noObjectInSightSignal.Raise();
        }

        void UpdateCelestialInfo(string _name, string _type, string _desc)
        {
            if (_name == string.Empty)
            {
                nameStr = string.Empty;
                typeStr = string.Empty;
                descStr = string.Empty;
            }
            else
            {
                nameStr = _name;
                typeStr = _type;
                descStr = _desc;
            }
        }
    }

}
