﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class IntValue : ScriptableObject
{
    public int initialValue;
    public int defaultValue;
    public int RuntimeValue;

    public void OnAfterDeserialize() { defaultValue = RuntimeValue; }
}
