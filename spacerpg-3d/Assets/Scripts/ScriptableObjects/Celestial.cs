﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Celestial", menuName = "ScriptableObjects/Celestial")]
public class Celestial : ScriptableObject
{
    public string Name;
    public string Type;
    public string Description;
}
