﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class FloatValue : ScriptableObject{

    public float initialValue;
    public float defaultValue;
    public float RuntimeValue;

    public void OnAfterDeserialize() { defaultValue = RuntimeValue; }
}
