﻿using UnityEngine;

public class SpaceshipInput : MonoBehaviour
{
    [Range(-1, 1)]
    public float pitch;
    [Range(-1, 1)]
    public float yaw;
    [Range(-1, 1)]
    public float roll;
    [Range(0, 1)]
    public float throttle;

    private float throttleTarget;
    private const float THROTTLE_SPEED = 0.3f;

    private Vector2 leftStickMove;

    private ShipControls controls;

    private void Awake()
    {
        controls = new ShipControls();

        controls.Gameplay.Throttle.started += ctx => OnThrottlePressed();
        controls.Gameplay.Throttle.canceled += ctx => SetTargetThrottle();

        controls.Gameplay.Brake.started += ctx => OnBrakePressed();
        controls.Gameplay.Brake.canceled += ctx => SetTargetThrottle();

        controls.Gameplay.Move.performed += ctx => leftStickMove = ctx.ReadValue<Vector2>();
        controls.Gameplay.Move.canceled += ctx => leftStickMove = Vector2.zero;
    }

    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    private void FixedUpdate()
    {
        SetOrientation();

        UpdateThrottle();
    }

    private void SetOrientation()
    {
        pitch = leftStickMove.y;
        yaw = leftStickMove.x;

        roll = -leftStickMove.x * 0.5f;
    }

    void OnThrottlePressed()
    {
        throttleTarget = 1.0f;
    }

    void OnBrakePressed()
    {
        throttleTarget = 0.0f;
    }

    void SetTargetThrottle()
    {
        throttleTarget = throttle;
    }

    void UpdateThrottle()
    {
        throttle = Mathf.MoveTowards(throttle, throttleTarget, Time.deltaTime * THROTTLE_SPEED);
    }
}
