﻿using UnityEngine;

public class SpaceshipFX : MonoBehaviour
{
    public float thrustersFXStartSpeedMin;
    public float thrustersFXStartSpeedMax;

    public ParticleSystem thrustFX;

    private float thrustFXSpeed;

    private ParticleSystem.MainModule thrustFXModule;

    private Spaceship spaceship;

    private void Awake()
    {
        thrustFXModule = thrustFX.main;
        spaceship = GetComponent<Spaceship>();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        UpdateThrustersFX();
    }

    void UpdateThrustersFX()
    {
        thrustFXSpeed = spaceship.Throttle * (thrustersFXStartSpeedMax - thrustersFXStartSpeedMin) + thrustersFXStartSpeedMin;
        thrustFXModule.startSpeed = -1 * thrustFXSpeed;
    }
}
