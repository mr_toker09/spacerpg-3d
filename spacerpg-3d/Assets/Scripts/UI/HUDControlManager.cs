﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDControlManager : MonoBehaviour
{
    private MenuControls controls;

    private CelestialInfoController _celestialInfoController;
    private MapScreenController _mapController;

    private void Awake()
    {
        _celestialInfoController = FindObjectOfType<CelestialInfoController>();
        _mapController = FindObjectOfType<MapScreenController>();

        controls = new MenuControls();

        controls.HUD.ShowObjectInfo.started += ctx => OnShowInfoClicked();

        controls.HUD.ScanMap.started += ctx => OnScanMapStarted();
        controls.HUD.ScanMap.canceled += ctx => OnScanManCancelled();
    }
    private void OnEnable()
    {
        controls.HUD.Enable();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnShowInfoClicked()
    {
        if (_celestialInfoController.showInfoButton.activeInHierarchy)
        {
            _celestialInfoController.ShowCelestialInfo();
        }
    }

    void OnScanMapStarted()
    {
        _mapController.StartRadarScan();
    }

    void OnScanManCancelled()
    {
        _mapController.StopRadarScan();
    }
}
