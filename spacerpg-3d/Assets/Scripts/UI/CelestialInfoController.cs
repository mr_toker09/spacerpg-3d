﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CelestialInfoController : MonoBehaviour
{
    public GameObject showInfoButton;
    public GameObject celestialInfoContainer;
    public Text nameText;
    public Text typeText;
    public Text descText;
    private ObjectDetector _objectDetector;

    private MenuControls controls;

    private void Awake()
    {
        _objectDetector = FindObjectOfType<ObjectDetector>();
    }

    private void OnEnable()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        showInfoButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetCelestialInfo(string _name, string _type, string _desc)
    {
        if (_name == string.Empty || _type == string.Empty)
        {
            nameText.text = "";
            typeText.text = "";
            descText.text = "";
            celestialInfoContainer.SetActive(false);
        }
        else
        {
            nameText.text = _name;
            typeText.text = _type;
            descText.text = _desc;
            celestialInfoContainer.SetActive(true);
        }
    }

    public void OnObjectInSightRaised()
    {
        if (!celestialInfoContainer.activeInHierarchy)
        {
            showInfoButton.SetActive(true);
        }
    }
    public void OnNoObjectInSightRaised()
    {
        if (showInfoButton.activeInHierarchy)
        {
            showInfoButton.SetActive(false);
        }
    }

    public void ShowCelestialInfo()
    {
        StopCoroutine(DisableCelestialInfoButton());
        SetCelestialInfo(_objectDetector.nameStr, _objectDetector.typeStr, _objectDetector.descStr);
        showInfoButton.SetActive(false);
        StartCoroutine(DisableCelestialInfoButton());
    }

    private IEnumerator DisableCelestialInfoButton()
    {
        yield return new WaitForSeconds(7f);
        SetCelestialInfo(string.Empty, string.Empty, string.Empty);
    }

}
