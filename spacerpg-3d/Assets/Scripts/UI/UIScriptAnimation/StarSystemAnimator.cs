﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSystemAnimator : MonoBehaviour
{
    public GameObject[] planets;
    public float[] orbitSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < planets.Length; i++)
        {
            UpdateRotation(orbitSpeed[i], planets[i]);
        }
    }

    void UpdateRotation(float speed, GameObject obj)
    {
        float zAngle = 0f;
        zAngle += speed;
        Vector3 rot = new Vector3(0, 0, zAngle);
        obj.transform.Rotate(rot);
    }
}
