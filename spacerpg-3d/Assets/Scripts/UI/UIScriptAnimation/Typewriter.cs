﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Typewriter : MonoBehaviour
{
    public IEnumerator TypeActionText(string textToDisplay, Text displayField, float speed)
    {
        ResetActionText(displayField);
        foreach (char letter in textToDisplay.ToCharArray())
        {
            displayField.text += letter;
            yield return new WaitForSeconds(speed);
        }
    }

    public void ResetActionText(Text displayField)
    {
        displayField.text = "";
    }
}
