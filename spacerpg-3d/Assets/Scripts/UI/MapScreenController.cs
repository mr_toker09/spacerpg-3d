﻿using UnityEngine.UI;
using UnityEngine;

public class MapScreenController : MonoBehaviour
{
    public Vector2 minGameBoundaryPos;
    public Vector2 maxGameBoundaryPos;
    public Vector2 minMapBoundaryPos;
    public Vector2 maxMapBoundaryPos;

    public GameObject radar;
    public GameObject scanMapButton;

    public Spaceship playerShip;
    public GameObject playerShipIcon;

    public GameObject[] planets;
    public GameObject[] planetIcons;
    
    public GameObject[] spaceObjectIcons;

    public Typewriter notificationInfo;

    private Text notificationInfoText;

    private bool shouldScan;
    public bool Scanning { get => shouldScan; }

    private void Awake()
    {
        playerShip = FindObjectOfType<Spaceship>();
        notificationInfoText = notificationInfo.gameObject.GetComponent<Text>();
    }
    // Start is called before the first frame update
    void Start()
    {
        notificationInfo.ResetActionText(notificationInfoText);
        spaceObjectIcons[0].SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        SetPositionOnMap(playerShip.gameObject, playerShipIcon, true);

        for (int i = 0; i < planets.Length; i++)
        {
            SetPositionOnMap(planets[i], planetIcons[i]);
        }

        radar.SetActive(shouldScan);
        if (shouldScan)
        {
            radar.transform.Rotate(0.0f, 0.0f, -0.5f);
        }
    }

    void SetPositionOnMap(GameObject origObj, GameObject mapObj, bool rot = false)
    {
        Vector3 _shipPos = origObj.transform.position;

        float _xPos = (_shipPos.x - minGameBoundaryPos.x) / (maxGameBoundaryPos.x - minGameBoundaryPos.x);
        float _yPos = (_shipPos.z - minGameBoundaryPos.y) / (maxGameBoundaryPos.y - minGameBoundaryPos.y);

        float xPos = _xPos * (maxMapBoundaryPos.x - minMapBoundaryPos.x) + minMapBoundaryPos.x;
        float yPos = _yPos * (maxMapBoundaryPos.y - minMapBoundaryPos.y) + minMapBoundaryPos.y;

        Vector2 shipPos = new Vector2(xPos, yPos);
        mapObj.transform.localPosition = shipPos;

        if (rot)
        {
            var _angles = origObj.transform.rotation.eulerAngles;
            var angles = mapObj.transform.rotation.eulerAngles;
            angles.z = _angles.y;
            mapObj.transform.rotation = Quaternion.Euler(-angles);
        }
    }

    public void OnDistressBeaconRaised()
    {
        StartCoroutine(notificationInfo.TypeActionText("INCOMING DISTRESS SIGNAL", notificationInfoText, 0.1f));

        Invoke("DisableNoticationInfo", 10f);
    }

    void DisableNoticationInfo()
    {
        notificationInfo.ResetActionText(notificationInfoText);
    }

    public void ShowScanMapButton()
    {
        scanMapButton.SetActive(true);
    }

    public void StartRadarScan()
    {
        scanMapButton.SetActive(false);
        shouldScan = true;
    }

    public void StopRadarScan()
    {
        shouldScan = false;
    }

    public void ShowBeaconOnRadar()
    {
        spaceObjectIcons[0].SetActive(true);
    }
}
