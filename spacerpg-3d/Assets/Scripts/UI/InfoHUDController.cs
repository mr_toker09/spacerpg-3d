﻿using UnityEngine.UI;
using UnityEngine;

public class InfoHUDController : MonoBehaviour
{
    public GameObject speedBar;
    public GameObject[] speedBars;

    public Spaceship spaceship;

    private void Awake()
    {
        spaceship = FindObjectOfType<Spaceship>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateSpeedBar();
    }

    void UpdateSpeedBar()
    {
        if (spaceship.Throttle == 0)
        {
            speedBar.SetActive(false);
        }
        else
        {
            speedBar.SetActive(true);
        }


        for (int i = 0; i < speedBars.Length; i++)
        {
            speedBars[i].SetActive(false);
        }

        for (int i = 0; i < (int)(spaceship.Throttle*10); i++)
        {
            speedBars[i].SetActive(true);
        }
    }
}
