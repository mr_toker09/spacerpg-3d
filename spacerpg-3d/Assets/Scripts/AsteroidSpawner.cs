﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour
{
    public GameObject asteroidPrefab;
    public int cubeDensity;
    public int seed;

    [Header("Sphere field")]
    public int sphereRadius;

    [Header("Ring field")]
    public float innerRadius;
    public float outerRadius;
    public float height;

    [Header("Asteroid Settings")]
    public float minSize;
    public float maxSize;

    private void Start()
    {
        Random.InitState(seed);
        GenerateSphereField();
    }

    void GenerateSphereField()
    {
        for (int i = 0; i < cubeDensity; i++)
        {
            GameObject _asteroid = Instantiate(asteroidPrefab, Random.insideUnitSphere * sphereRadius, Random.rotation);
            _asteroid.transform.SetParent(transform);
            float _scale = Random.Range(minSize, maxSize);
            _asteroid.transform.localScale = new Vector3(_scale, _scale, _scale);
        }
    }
}
