// GENERATED AUTOMATICALLY FROM 'Assets/InputActions/MenuControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @MenuControls : IInputActionCollection, IDisposable
{
    private InputActionAsset asset;
    public @MenuControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""MenuControls"",
    ""maps"": [
        {
            ""name"": ""HUD"",
            ""id"": ""1f1d9b16-f8ea-437a-9fe6-4cc407f9f63b"",
            ""actions"": [
                {
                    ""name"": ""ShowObjectInfo"",
                    ""type"": ""Button"",
                    ""id"": ""b4b5bba0-aa30-4f7d-949e-727c5bc1782f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ScanMap"",
                    ""type"": ""Button"",
                    ""id"": ""1b3fd2de-e077-4db2-805b-7b9739871d20"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""3f9a6ca1-be39-4415-b147-c945909d0f7a"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ShowObjectInfo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""89ba72fe-56d1-425a-88e4-db7421f033b0"",
                    ""path"": ""<Keyboard>/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ShowObjectInfo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4b89daa9-9d3d-4a5a-a4d9-cfabcdc83da8"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ScanMap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""504cd2c9-eeb6-447b-8ae7-35f5445b2078"",
                    ""path"": ""<Keyboard>/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ScanMap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // HUD
        m_HUD = asset.FindActionMap("HUD", throwIfNotFound: true);
        m_HUD_ShowObjectInfo = m_HUD.FindAction("ShowObjectInfo", throwIfNotFound: true);
        m_HUD_ScanMap = m_HUD.FindAction("ScanMap", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // HUD
    private readonly InputActionMap m_HUD;
    private IHUDActions m_HUDActionsCallbackInterface;
    private readonly InputAction m_HUD_ShowObjectInfo;
    private readonly InputAction m_HUD_ScanMap;
    public struct HUDActions
    {
        private @MenuControls m_Wrapper;
        public HUDActions(@MenuControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @ShowObjectInfo => m_Wrapper.m_HUD_ShowObjectInfo;
        public InputAction @ScanMap => m_Wrapper.m_HUD_ScanMap;
        public InputActionMap Get() { return m_Wrapper.m_HUD; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(HUDActions set) { return set.Get(); }
        public void SetCallbacks(IHUDActions instance)
        {
            if (m_Wrapper.m_HUDActionsCallbackInterface != null)
            {
                @ShowObjectInfo.started -= m_Wrapper.m_HUDActionsCallbackInterface.OnShowObjectInfo;
                @ShowObjectInfo.performed -= m_Wrapper.m_HUDActionsCallbackInterface.OnShowObjectInfo;
                @ShowObjectInfo.canceled -= m_Wrapper.m_HUDActionsCallbackInterface.OnShowObjectInfo;
                @ScanMap.started -= m_Wrapper.m_HUDActionsCallbackInterface.OnScanMap;
                @ScanMap.performed -= m_Wrapper.m_HUDActionsCallbackInterface.OnScanMap;
                @ScanMap.canceled -= m_Wrapper.m_HUDActionsCallbackInterface.OnScanMap;
            }
            m_Wrapper.m_HUDActionsCallbackInterface = instance;
            if (instance != null)
            {
                @ShowObjectInfo.started += instance.OnShowObjectInfo;
                @ShowObjectInfo.performed += instance.OnShowObjectInfo;
                @ShowObjectInfo.canceled += instance.OnShowObjectInfo;
                @ScanMap.started += instance.OnScanMap;
                @ScanMap.performed += instance.OnScanMap;
                @ScanMap.canceled += instance.OnScanMap;
            }
        }
    }
    public HUDActions @HUD => new HUDActions(this);
    public interface IHUDActions
    {
        void OnShowObjectInfo(InputAction.CallbackContext context);
        void OnScanMap(InputAction.CallbackContext context);
    }
}
